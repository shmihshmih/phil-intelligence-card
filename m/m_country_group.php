<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'country_group') {
        if($_POST['country_group_id'] == '') {
            $_POST['country_group_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_country_group8mod(:id,
                                                                  :caption)");
        $stmt->execute(array('id' => $_POST['country_group_id'],
                             'caption' => $_POST['country_group_caption']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление группы стран
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'country_group') {
        $stmt = $connect->prepare("SELECT $db.f_country_group8del(?)");
        $stmt->bindValue(1, $_POST['country_group_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //пполучение групп стран
    if(isset($_POST['choose_country_group'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_country_group v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_country_group'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['country_group_id'] = $row['id'];
        $data['country_group_caption'] = $row['caption']?:'';
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>