<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'school') {
        if($_POST['school_id'] == '') {
            $_POST['school_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_school8mod(:id,
                                                           :school,
                                                           :annotation,
                                                           :parent,
                                                           :bibliography)");
        $stmt->execute(array('id' => $_POST['school_id'],
                             'school' => $_POST['school_caption'],
                             'annotation' => $_POST['school_annotation'],
                             'parent' => $_POST['school_parent']?:null,
                             'bibliography' => $_POST['school_bibliography']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление школы
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'school') {
        $stmt = $connect->prepare("SELECT $db.f_school8del(?)");
        $stmt->bindValue(1, $_POST['school_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //получение школы
    if(isset($_POST['choose_school'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_school v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_school'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['school_id'] = $row['id'];
        $data['school_caption'] = $row['school'];
        $data['school_annotation'] = $row['annotation'];
        $data['school_parent'] = $row['parent']?:'';
        $data['school_bibliography'] = $row['bibliography'];
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        }  
    }
?>
