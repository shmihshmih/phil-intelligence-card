<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'opus') {
        if($_POST['opus_id'] == '') {
            $_POST['opus_id'] = null;
        }
    $stmt = $connect->prepare("SELECT $db.f_opus8mod(:id, 
                                                     :pid, 
                                                     :caption, 
                                                     :annotation, 
                                                     :cover, 
                                                     :published, 
                                                     :bibliography, 
                                                     :key_word, 
                                                     :section)");
    $stmt->execute(array('id' => $_POST['opus_id'],
                         'pid' => $_POST['opus_pid']?:null,
                         'caption' => $_POST['opus_caption'],
                         'annotation' => $_POST['opus_annotation'],
                         'cover' => $_POST['opus_cover'],
                         'published' => $_POST['opus_published'],
                         'bibliography' => $_POST['opus_bibliography'],
                         'key_word' => $_POST['opus_key_word'],
                         'section' => $_POST['section_opus_list']?:null));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление произведения
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'opus') {
        $stmt = $connect->prepare("SELECT $db.f_opus8del(?)");
        $stmt->bindValue(1, $_POST['opus_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //получение произведения
    if(isset($_POST['choose_opus'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_opus v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_opus'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();
        
        $data['opus_id'] = $row['id'];
        $data['opus_pid'] = $row['pid']?:'';
        $data['opus_caption'] = $row['caption'];
        $data['opus_annotation'] = $row['annotation'];
        $data['opus_cover'] = $row['cover'];
        $data['opus_published'] = $row['published'];
        $data['opus_bibliography'] = $row['bibliography'];
        $data['opus_key_word'] = $row['key_word'];
        $data['opus_section'] = $row['section']?:'';
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>