<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'country') {
        if($_POST['country_id'] == '') {
            $_POST['country_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_country8mod(:id,
                                                            :pid,
                                                            :country,
                                                            :language)");
        $stmt->execute(array('id' => $_POST['country_id'],
                             'pid' => $_POST['country_group']?:null,
                             'country' => $_POST['country_caption'],
                             'language' => $_POST['language']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление стран
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'country') {
        $stmt = $connect->prepare("SELECT $db.f_country8del(?)");
        $stmt->bindValue(1, $_POST['country_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //пполучение стран
    if(isset($_POST['choose_country'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_country v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_country'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['country_id'] = $row['id'];
        $data['country_group'] = $row['pid']?:'';
        $data['country_caption'] = $row['country'];
        $data['country_language'] = $row['language'];
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>