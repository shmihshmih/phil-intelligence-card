<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'files') {
        if($_POST['files_id'] == '') {
            $_POST['files_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_files8mod(:id,
                                                            :pid,
                                                            :caption,
                                                            :author,
                                                            :link,
                                                            :bibliography,
                                                            :type,
                                                            :key_word,
                                                            :format)");
        $stmt->execute(array('id' => $_POST['files_id'],
                             'pid' => $_POST['files_pid']?:null,
                             'caption' => $_POST['files_caption'],
                             'author' => $_POST['files_author'],
                             'link' => $_POST['files_link'],
                             'bibliography' => $_POST['files_bibliography'],
                             'type' => $_POST['files_type'],
                             'key_word' => $_POST['files_key_word'],
                             'format' => $_POST['files_format']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление файла
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'files') {
        $stmt = $connect->prepare("SELECT $db.f_files8del(?)");
        $stmt->bindValue(1, $_POST['files_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //пполучение файла
    if(isset($_POST['choose_files'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_files v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_files'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['files_id'] = $row['id'];
        $data['files_pid'] = $row['pid']?:'';
        $data['files_caption'] = $row['caption'];
        $data['files_author'] = $row['author'];
        $data['files_link'] = $row['link'];
        $data['files_bibliography'] = $row['bibliography'];
        $data['files_type'] = $row['type'];
        $data['files_key_word'] = $row['key_word'];
        $data['files_format'] = $row['format'];
        
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>