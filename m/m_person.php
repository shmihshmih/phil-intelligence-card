<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'person') {
        if($_POST['person_id'] == '') {
            $_POST['person_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_person8mod(:id,
                                                           :name,
                                                           :surname,
                                                           :last_name,
                                                           :birth_date,
                                                           :death_date,
                                                           :country,
                                                           :parent,
                                                           :school,
                                                           :key_word,
                                                           :annotation,
                                                           :link,
                                                           :portrait,
                                                           :bibliography)");
        $stmt->execute(array('id' => $_POST['person_id'],
                             'name' => $_POST['person_name'],
                             'surname' => $_POST['person_surname'],
                             'last_name' => $_POST['person_last_name'],
                             'birth_date' => $_POST['person_birth_date'],
                             'death_date' => $_POST['person_death_date'],
                             'country' => $_POST['person_country_list']?:null,
                             'parent' => $_POST['person_person_list']?:null,
                             'school' => $_POST['person_school_list']?:null,
                             'key_word' => $_POST['person_key_word'],
                             'annotation' => $_POST['person_annotation'],
                             'link' => $_POST['person_link'],
                             'portrait' => $_POST['person_portrait'],
                             'bibliography' => $_POST['person_bibliography']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    };
    //удаление персоны
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'person') {
        $stmt = $connect->prepare("SELECT $db.f_person8del(?)");
        $stmt->bindValue(1, $_POST['person_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    };
    //получение персоны
    if(isset($_POST['choose_person'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_person v WHERE v.id = ?");
        $stmt->bindValue(1, $_POST['choose_person'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['id'] = $row['id'];
        $data['name'] = $row['name'];
        $data['surname'] = $row['surname'];
        $data['last_name'] = $row['last_name'];
        $data['birth_date'] = $row['birth_date'];
        $data['death_date'] = $row['death_date'];
        $data['country'] = $row['country_id']?:'';
        $data['parent'] = $row['papa_person_id']?:'';
        $data['school'] = $row['school_id']?:'';
        $data['key_word'] = $row['person_key_word'];
        $data['annotation'] = $row['person_annotation'];
        $data['link'] = $row['link'];
        $data['portrait'] = $row['portrait'];
        $data['bibliography'] = $row['person_bibliography'];
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    };
?>
