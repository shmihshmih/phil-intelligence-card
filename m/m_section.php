<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'section') {
        if($_POST['section_id'] == '') {
            $_POST['section_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_section8mod(:id,
                                                           :pid,
                                                           :caption,
                                                           :annotation)");
        $stmt->execute(array('id' => $_POST['section_id'],
                             'pid' => $_POST['section_parent']?:null,
                             'caption' => $_POST['section_caption'],
                             'annotation' => $_POST['section_annotation']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление раздела науки
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'section') {
        $stmt = $connect->prepare("SELECT $db.f_section8del(?)");
        $stmt->bindValue(1, $_POST['section_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //получение разделов науки
    if(isset($_POST['choose_section'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_section v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_section'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['section_id'] = $row['id'];
        $data['section_parent'] = $row['pid']?:'';
        $data['section_caption'] = $row['caption'];
        $data['section_annotation'] = $row['annotation'];
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>