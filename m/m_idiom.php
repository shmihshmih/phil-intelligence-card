<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'idiom') {
        if($_POST['idiom_id'] == '') {
            $_POST['idiom_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_idiom8mod(:id,
                                                            :pid,
                                                            :idiom,
                                                            :original)");
        $stmt->execute(array('id' => $_POST['idiom_id'],
                             'pid' => $_POST['idiom_pid']?:null,
                             'idiom' => $_POST['idiom_idiom'],
                             'original' => $_POST['idiom_original']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление высказывания
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'idiom') {
        $stmt = $connect->prepare("SELECT $db.f_idiom8del(?)");
        $stmt->bindValue(1, $_POST['idiom_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //пполучение высказываний
    if(isset($_POST['choose_idiom'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_idiom v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_idiom'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['idiom_id'] = $row['id'];
        $data['idiom_pid'] = $row['pid']?:'';
        $data['idiom_idiom'] = $row['idiom'];
        $data['idiom_original'] = $row['original'];
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>