<?php
    require_once('../db.php');
    //редактирование или добавление
    if(isset($_POST['form_kind']) && $_POST['form_kind'] == 'dictionary') {
        if($_POST['dictionary_id'] == '') {
            $_POST['dictionary_id'] = null;
        }
        $stmt = $connect->prepare("SELECT $db.f_dictionary8mod(:id,
                                                               :pid,
                                                               :parent,
                                                               :caption,
                                                               :description,
                                                               :bibliography,
                                                               :foreign_lang)");
        $stmt->execute(array('id' => $_POST['dictionary_id'],
                             'pid' => $_POST['person_dictionary_list']?:null,
                             'parent' => $_POST['dictionary_dictionary_list']?:null,
                             'caption' => $_POST['dictionary_caption'],
                             'description' => $_POST['dictionary_description'],
                             'bibliography' => $_POST['dictionary_bibliography'],
                             'foreign_lang' => $_POST['foreign_lang']));
        $err = $stmt->fetch();
        if(isset($err[2]) == false) {
        echo "<html>
                <head>
                <meta http-equiv='Refresh' content='0; URL=".$_SERVER['HTTP_REFERER']."'>
                </head>
             </html>";
        } else {
            echo $err[2];
        }
    }
    //удаление dict
    if(isset($_POST['del_kind']) && $_POST['del_kind'] == 'dictionary') {
        $stmt = $connect->prepare("SELECT $db.f_dictionary8del(?)");
        $stmt->bindValue(1, $_POST['dictionary_id'], PDO::PARAM_INT);
        $stmt->execute();
        $arr = $stmt->errorInfo();
        echo json_encode($arr);
    }
    //пполучение стран
    if(isset($_POST['choose_dictionary'])) {
        $stmt = $connect->prepare("SELECT v.* from $db.v_dictionary v where v.id = ?");
        $stmt->bindValue(1, $_POST['choose_dictionary'], PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $data['dictionary_id'] = $row['id'];
        $data['dictionary_pid'] = $row['pid']?:'';
        $data['dictionary_parent'] = $row['parent']?:'';
        $data['dictionary_caption'] = $row['caption'];
        $data['dictionary_description'] = $row['description'];
        $data['dictionary_bibliography'] = $row['bibliography'];
        $data['dictionary_foreign_lang'] = $row['foreign_lang'];
        $err = $stmt->fetch();
        if($err == null) {
            echo json_encode($data);
        } else {
            echo "<script>alert($err[2]);</script>";
        } 
    }
?>