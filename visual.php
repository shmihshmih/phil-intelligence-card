<!DOCTYPE html>
<html>
<head>
  <title>Визуализация</title>
  <script type="text/javascript" src="js/main.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
  <script src="vis-4.17.0/dist/vis.js"></script>
  <link href="vis-4.17.0/dist/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />
  <link href="css/visualisation.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="visualization"></div>
  <script src="js/v_timeline.js"></script>
</body>
</html>