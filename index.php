<?php
require_once('setting.php');
require_once('db.php');
?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="UTF-8">
		<title>Форма администрирования</title>
		<link rel="stylesheet" href="css/reset.css" media="screen" type="text/css" />
		<link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/m_person.js"></script>
		<script type="text/javascript" src="js/m_school.js"></script>
		<script type="text/javascript" src="js/m_opus.js"></script>
		<script type="text/javascript" src="js/m_section.js"></script>
		<script type="text/javascript" src="js/m_country_group.js"></script>
		<script type="text/javascript" src="js/m_country.js"></script>
		<script type="text/javascript" src="js/m_dictionary.js"></script>
		<script type="text/javascript" src="js/m_idiom.js"></script>
		<script type="text/javascript" src="js/m_files.js"></script>
</head>
<body>
<div class="content">
	<div class="forms">
	<div class="person_mod">
	<h1>Персона	<div class="nav">
		<div class="nav_item" onclick='toggleBlock("person_new");cleaner("person_new");'></div>
		<div class="nav_item" onclick='toggleBlock("person_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="person_new" action='m/m_person.php'  method="post">
             <input hidden="hidden" name="form_kind" value="person"/>
             <input type="text" hidden="hidden" name="person_id">
			 <input type="text" name="person_name" placeholder="Имя" required/>
			 <input type="text" name="person_surname" placeholder="Фамилия" />
			 <input type="text" name="person_last_name" placeholder="Отчество" />
			 <br>
			<input type="date" name="person_birth_date" required/>
			<br>
			<input type="date" name="person_death_date" required/>
			<br>
			<select name="person_country_list">
                <?php include('v/v_person_country_list.php'); ?>
			</select>
			<br>
			<select name="person_person_list">
				 <?php include('v/v_person_person_list.php'); ?>
			</select>
			<br>
			<select name="person_school_list">
				 <?php include('v/v_person_school_list.php'); ?>
			</select>
			<br>
			 <textarea name="person_key_word" maxlength="3000" placeholder="Ключевые слова"></textarea>
			 <textarea name="person_annotation" maxlength="3000" placeholder="Аннотация"></textarea>
			 <input type="text" name="person_link" placeholder="сылка подробнее" />
			 <input type="text" name="person_portrait" placeholder="Портрет" />
			 <textarea name="person_bibliography" maxlength="3000" placeholder="Библиография"></textarea>
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>
    <form id="person_edit" action="" method="post">
        <?php include_once('v/v_person.php'); ?>
	</form>
	</fieldset>
</div>

<div class="files_mod">
	<h1>Файлы		<div class="nav">
		<div class="nav_item" onclick='toggleBlock("files_new");cleaner("files_new");'></div>
		<div class="nav_item" onclick='toggleBlock("files_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="files_new" action="m/m_files.php" method="post">
            <input hidden="hidden" name="form_kind" value="files"/>
            <input type="text" hidden="hidden" name="files_id">
            <select name="files_pid">
                <?php include('v/v_files_opus.php'); ?>
            </select>
            <input type="text" name="files_caption" placeholder="Название" required>
            <input type="text" name="files_author" placeholder="Автор">
            <textarea name="files_link"  maxlength="3000" placeholder="Ссылка" required></textarea>
            <textarea name="files_bibliography"  maxlength="3000" placeholder="Библиография"></textarea>
            <input type="text" name="files_type" placeholder="Тип документа(ГОСТ и тд.)" required>
            <textarea name="files_key_word"  maxlength="3000" placeholder="Ключевые слова"></textarea>
            <input type="text" name="files_format" placeholder="Расширение файла">
			<input type="submit" value="Сделать">
			<inout type="reset" value=" ">
	</form>
		<form id="files_edit" action="" method="post">
            <?php include_once('v/v_files.php'); ?>
	    </form>
	</fieldset>
</div>

<div class="opus_mod">
	<h1>
	    <div class="tab_changer" onclick='tabForm("files_mod");'></div>
	    Произведение		<div class="nav">
		<div class="nav_item" onclick='toggleBlock("opus_new");cleaner("opus_new");'></div>
		<div class="nav_item" onclick='toggleBlock("opus_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="opus_new" action="m/m_opus.php" method="post">
            <input hidden="hidden" name="form_kind" value="opus"/>
            <input type="text" hidden="hidden" name="opus_id">
			<select name="opus_pid" required>
				<?php include('v/v_person_person_list.php'); ?>
			</select>
			<br>
			 <input type="text" name="opus_caption" placeholder="Название произведения" required/>
			 <textarea name="opus_annotation" maxlength="3000" placeholder="Аннотация" required></textarea>
			 <input type="text" name="opus_cover" placeholder="Обложка" />
			 <br>
			 <input type="date" name="opus_published" placeholder="Опубликована"/>
			 <textarea name="opus_bibliography" maxlength="3000" placeholder="Библиография"></textarea>
			 <textarea name="opus_key_word" maxlength="3000" placeholder="Ключевые слова"></textarea>
			 <br>
			<select name="section_opus_list">
				<?php include('v/v_opus_section.php'); ?>
			</select>
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>
		<form id="opus_edit" action="" method="post">
            <?php include_once('v/v_opus.php'); ?>
	</form>
	</fieldset>
</div>

<div class="idiom_mod">
	<h1>Высказывания		<div class="nav">
		<div class="nav_item" onclick='toggleBlock("idiom_new");cleaner("idiom_new");'></div>
		<div class="nav_item" onclick='toggleBlock("idiom_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="idiom_new" action="m/m_idiom.php" method="post">
            <input hidden="hidden" name="form_kind" value="idiom"/>
            <input type="text" hidden="hidden" name="idiom_id">
            <select type="text" name="idiom_pid">
                <?php include('v/v_person_person_list.php'); ?>
            </select>    
            <input type="text" name="idiom_idiom" placeholder="Высказывание" required>
            <input type="text" placeholder="Оригинал" name="idiom_original">
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>
		<form id="idiom_edit" action="" method="post">
            <?php include_once('v/v_idiom.php'); ?>
	    </form>
	</fieldset>
</div>

<div class="dictionary_mod">
	<h1>
	        <div class="tab_changer" onclick='tabForm("idiom_mod");'></div>
	    Словарь	<div class="nav">
		<div class="nav_item" onclick='toggleBlock("dictionary_new");cleaner("dictionary_new");'></div>
		<div class="nav_item" onclick='toggleBlock("dictionary_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="dictionary_new" action="m/m_dictionary.php" method="post">
            <input hidden="hidden" name="form_kind" value="dictionary"/>
            <input type="text" hidden="hidden" name="dictionary_id">
			<select name="person_dictionary_list">
				<?php include('v/v_person_person_list.php'); ?>
			</select>
			<br>
			<select name="dictionary_dictionary_list">
				<?php include('v/v_dictionary_list.php'); ?>
			</select>
			<br>
			 <input type="text" name="dictionary_caption" placeholder="Понятие" required/>
			 <textarea name="dictionary_description" maxlength="3000" placeholder="Описание" required></textarea>
			 <textarea name="dictionary_bibliography" maxlength="3000" placeholder="Библиография"></textarea>
			 <input type="text" name="foreign_lang" placeholder="Другой язык" />
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>
		<form id="dictionary_edit" action="" method="post">
        <?php include_once('v/v_dictionary.php'); ?>
	</form>
	</fieldset>
</div>

<div class="country_group_mod">
	<h1>Группы стран	<div class="nav">
		<div class="nav_item" onclick='toggleBlock("country_group_new");cleaner("country_group_new");'></div>
		<div class="nav_item" onclick='toggleBlock("country_group_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="country_group_new" action="m/m_country_group.php" method="post">
             <input hidden="hidden" name="form_kind" value="country_group"/>
             <input type="text" hidden="hidden" name="country_group_id"/>
			 <input type="text" name="country_group_caption" placeholder="Название" required/>
			 <input type="submit" value="Сделать">
			 <input type="reset" value=" ">
	</form>
	<form id="country_group_edit" action="" method="post">
        <?php include_once('v/v_country_group.php'); ?>
	</form>
	</fieldset>
</div>

<div class="country_mod">
    <h1>
        <div class="tab_changer" onclick='tabForm("country_group_mod");'></div>
        Страны	
        <div class="nav">
		    <div class="nav_item" onclick='toggleBlock("country_new");cleaner("country_new");'></div>
		    <div class="nav_item" onclick='toggleBlock("country_edit");'></div>
	    </div>
	</h1>
	<fieldset>
	<form id="country_new" action="m/m_country.php" method="post">
                     <input hidden="hidden" name="form_kind" value="country"/>
            <input type="text" hidden="hidden" name="country_id"/>
            <select name="country_group">
				<?php include('v/v_country_country_group.php'); ?>
            </select>
			<br>
			 <input type="text" name="country_caption" placeholder="Название" required/>
			 <input type="text" name="language" placeholder="Язык" required/>
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>

	<form id="country_edit" action="" method="post">
        <?php include_once('v/v_country.php'); ?>
	</form>
	</fieldset>
</div>

<div class="school_mod">
	<h1>Школы    <div class="nav">
		<div class="nav_item" onclick='toggleBlock("school_new");cleaner("school_new");'></div>
		<div class="nav_item" onclick='toggleBlock("school_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="school_new" action="m/m_school.php" method="post">
             <input hidden="hidden" name="form_kind" value="school"/>
             <input type="text" hidden="hidden" name="school_id"/>
			 <input type="text" name="school_caption" placeholder="Школа" required/>
			 <textarea name="school_annotation" maxlength="3000" placeholder="Аннтотация" required></textarea>
			<select name="school_parent">
				<?php include('v/v_person_school_list.php'); ?>
			</select>
			<br>
			<textarea name="school_bibliography" maxlength="3000" placeholder="Библиография"></textarea>
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>
		<form id="school_edit" action="" method="post">
            <?php include_once('v/v_school.php'); ?>
	</form>
	</fieldset>
</div>

<div class="section_mod">
	<h1>Разделы науки	<div class="nav">
		<div class="nav_item" onclick='toggleBlock("section_new");cleaner("section_new");'></div>
		<div class="nav_item" onclick='toggleBlock("section_edit");'></div>
	</div></h1>
	<fieldset>
	<form id="section_new" action="m/m_section.php" method="post">
             <input hidden="hidden" name="form_kind" value="section"/>
             <input type="text" hidden="hidden" name="section_id"/>
			<select name="section_parent">
				<?php include('v/v_opus_section.php')?>
			</select>
			<br>
			 <input type="text" name="section_caption" placeholder="Название" required/>
			 <textarea name="section_annotation" maxlength="3000" placeholder="Аннотация" required></textarea>
			<input type="submit" value="Сделать">
			<input type="reset" value=" ">
	</form>
		<form id="section_edit" action="" method="post">
	        <?php include_once('v/v_section.php'); ?>
	</form>
	</fieldset>
</div>

<div class="menu_mod">
   <h1>Меню</h1>
    <hr>
    <p class="menu_item"><a href="visual.php">Философия: Визулизация </a></p>
    <hr>
    <p class="menu_item">Философия: Управление</p>
    <hr>
    <p class="menu_item" onclick='toggleBlock("philosophy_log");'><a href="philosophy_log.php">Философия: Лог</a></p>
    <hr>
</div>
</div>
</div>
