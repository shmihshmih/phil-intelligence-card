/* SQL Manager Lite for MySQL                              5.6.3.48526 */
/* ------------------------------------------------------------------- */
/* Host     : localhost                                                */
/* Port     : 3306                                                     */
/* Database : philosophy                                               */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `philosophy`;

CREATE DATABASE `philosophy`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `philosophy`;

/* Удаление объектов БД */

DROP VIEW IF EXISTS `v_section`;
DROP VIEW IF EXISTS `v_school`;
DROP VIEW IF EXISTS `v_person`;
DROP VIEW IF EXISTS `v_opus`;
DROP VIEW IF EXISTS `v_idiom`;
DROP VIEW IF EXISTS `v_files`;
DROP VIEW IF EXISTS `v_dictionary`;
DROP VIEW IF EXISTS `v_country_group`;
DROP VIEW IF EXISTS `v_country`;
DROP FUNCTION IF EXISTS `f_section8mod`;
DROP FUNCTION IF EXISTS `f_section8upd`;
DROP FUNCTION IF EXISTS `f_section8del`;
DROP FUNCTION IF EXISTS `f_section8add`;
DROP FUNCTION IF EXISTS `f_school8mod`;
DROP FUNCTION IF EXISTS `f_school8upd`;
DROP FUNCTION IF EXISTS `f_school8del`;
DROP FUNCTION IF EXISTS `f_school8add`;
DROP FUNCTION IF EXISTS `f_person8mod`;
DROP FUNCTION IF EXISTS `f_person8upd`;
DROP FUNCTION IF EXISTS `f_person8del`;
DROP FUNCTION IF EXISTS `f_person8add`;
DROP FUNCTION IF EXISTS `f_opus8mod`;
DROP FUNCTION IF EXISTS `f_opus8upd`;
DROP FUNCTION IF EXISTS `f_opus8del`;
DROP FUNCTION IF EXISTS `f_opus8add`;
DROP FUNCTION IF EXISTS `f_idiom8mod`;
DROP FUNCTION IF EXISTS `f_idiom8upd`;
DROP FUNCTION IF EXISTS `f_idiom8del`;
DROP FUNCTION IF EXISTS `f_idiom8add`;
DROP FUNCTION IF EXISTS `f_files8mod`;
DROP FUNCTION IF EXISTS `f_files8upd`;
DROP FUNCTION IF EXISTS `f_files8del`;
DROP FUNCTION IF EXISTS `f_files8add`;
DROP FUNCTION IF EXISTS `f_dictionary8mod`;
DROP FUNCTION IF EXISTS `f_dictionary8upd`;
DROP FUNCTION IF EXISTS `f_dictionary8del`;
DROP FUNCTION IF EXISTS `f_dictionary8add`;
DROP FUNCTION IF EXISTS `f_country_group8mod`;
DROP FUNCTION IF EXISTS `f_country_group8upd`;
DROP FUNCTION IF EXISTS `f_country_group8del`;
DROP FUNCTION IF EXISTS `f_country_group8add`;
DROP FUNCTION IF EXISTS `f_country8mod`;
DROP FUNCTION IF EXISTS `f_country8upd`;
DROP FUNCTION IF EXISTS `f_country8del`;
DROP FUNCTION IF EXISTS `f_country8add`;
DROP FUNCTION IF EXISTS `f_core_get_id`;
DROP TABLE IF EXISTS `t_idiom`;
DROP TABLE IF EXISTS `t_files`;
DROP TABLE IF EXISTS `t_opus`;
DROP TABLE IF EXISTS `t_section`;
DROP TABLE IF EXISTS `t_person`;
DROP TABLE IF EXISTS `t_school`;
DROP TABLE IF EXISTS `t_dictionary`;
DROP TABLE IF EXISTS `t_country`;
DROP TABLE IF EXISTS `t_country_group`;
DROP TABLE IF EXISTS `core_user`;
DROP TABLE IF EXISTS `core_seq`;
DROP TABLE IF EXISTS `core_roles`;
DROP TABLE IF EXISTS `core_log`;

/* Структура для таблицы `core_log`:  */

CREATE TABLE `core_log` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентифитор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Идентификатор записи',
  `message` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Сообщение изменения',
  `table` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Изменяемая таблица',
  `timed` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время изменения',
  `action_type` VARCHAR(20) COLLATE utf8_general_ci NOT NULL COMMENT 'Тип изменения (i,d,u)',
  `script` VARCHAR(12000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Скрипт для возврата'
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_roles`:  */

CREATE TABLE `core_roles` (
  `id` BIGINT(20) NOT NULL,
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `active` TINYINT(1) NOT NULL
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_seq`:  */

CREATE TABLE `core_seq` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `st` BIGINT(20) NOT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=122 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_user`:  */

CREATE TABLE `core_user` (
  `id` BIGINT(20) NOT NULL,
  `login` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `pass` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `active` TINYINT(1) NOT NULL
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_country_group`:  */

CREATE TABLE `t_country_group` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT 'Название группы стран',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_country`:  */

CREATE TABLE `t_country` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Группа стран',
  `country` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Страна',
  `language` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Язык',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  CONSTRAINT `t_country_fk1` FOREIGN KEY (`pid`) REFERENCES `t_country_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_dictionary`:  */

CREATE TABLE `t_dictionary` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Автор термина',
  `parent` BIGINT(20) DEFAULT NULL COMMENT 'Предшествующий термин',
  `caption` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Термин',
  `description` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Описание термина',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Библиографическое описание',
  `foreign_lang` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'На др. языке',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  KEY `parent` USING BTREE (`parent`),
  CONSTRAINT `t_dictionary_fk1` FOREIGN KEY (`parent`) REFERENCES `t_dictionary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_dictionary_fk2` FOREIGN KEY (`pid`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_school`:  */

CREATE TABLE `t_school` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `school` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Школа',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Краткое описание',
  `parent` BIGINT(20) DEFAULT NULL COMMENT 'Предыдущее учение',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Библиографическое описание',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `parent` USING BTREE (`parent`),
  KEY `annotation` USING BTREE (`annotation`(255)),
  CONSTRAINT `t_school_fk1` FOREIGN KEY (`parent`) REFERENCES `t_school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_person`:  */

CREATE TABLE `t_person` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `name` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Имя',
  `surname` VARCHAR(30) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Фамилия',
  `last_name` VARCHAR(30) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Отчество',
  `birth_date` DATE NOT NULL COMMENT 'Дата рождения',
  `death_date` DATE NOT NULL COMMENT 'Дата смерти',
  `country` BIGINT(20) DEFAULT NULL COMMENT 'Страна-язык-национальность',
  `parent` BIGINT(20) DEFAULT NULL COMMENT 'Учитель, наставник',
  `school` BIGINT(20) DEFAULT NULL COMMENT 'Школа',
  `key_word` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Ключевые слова',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Краткое описание',
  `link` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Ссылка на биографию',
  `portrait` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Портрет',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Бибдиографическое описание',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `country` USING BTREE (`country`),
  KEY `parent` USING BTREE (`parent`),
  KEY `section` USING BTREE (`school`),
  CONSTRAINT `t_person_fk1` FOREIGN KEY (`country`) REFERENCES `t_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_person_fk2` FOREIGN KEY (`school`) REFERENCES `t_school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_person_fk3` FOREIGN KEY (`parent`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_section`:  */

CREATE TABLE `t_section` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Вышестоящий раздел',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT 'Название',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Описание',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  KEY `annotation` USING BTREE (`annotation`(255)),
  CONSTRAINT `t_section_fk1` FOREIGN KEY (`pid`) REFERENCES `t_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_opus`:  */

CREATE TABLE `t_opus` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) NOT NULL COMMENT 'Автор произведения',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT 'Название',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Аннотация',
  `cover` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Обложка',
  `published` DATE DEFAULT NULL COMMENT 'Опубликовано',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Библиографическое описание',
  `key_word` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Ключевые слова',
  `section` BIGINT(20) DEFAULT NULL COMMENT 'Раздел науки',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  KEY `section` USING BTREE (`section`),
  KEY `annotation` USING BTREE (`annotation`(255)),
  CONSTRAINT `t_opus_fk1` FOREIGN KEY (`pid`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_opus_fk2` FOREIGN KEY (`section`) REFERENCES `t_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_files`:  */

CREATE TABLE `t_files` (
  `id` BIGINT(20) NOT NULL,
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Произведение',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `author` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Автор',
  `link` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL,
  `add_date` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visible` TINYINT(1) NOT NULL,
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL,
  `type` VARCHAR(30) COLLATE utf8_general_ci NOT NULL COMMENT 'Тип файла(Гост,словарь и т.д)',
  `key_word` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL,
  `format` VARCHAR(30) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Формат файла',
  KEY `pid` USING BTREE (`pid`),
  CONSTRAINT `t_files_fk1` FOREIGN KEY (`pid`) REFERENCES `t_opus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_idiom`:  */

CREATE TABLE `t_idiom` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Автор',
  `idiom` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Высказывание',
  `original` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Оригинал',
  KEY `pid` USING BTREE (`pid`),
  CONSTRAINT `t_idiom_fk1` FOREIGN KEY (`pid`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Определение для функции `f_core_get_id`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_core_get_id`()
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT 'Последовательность идентификаторов'
BEGIN
  declare 
    new_id  bigint; 
  insert `core_seq`(`st`) VALUES(1);
  select max(`id`) into `new_id` from `core_seq` where `st` = 1;
  delete from `core_seq` where `st` = 0;
  update `core_seq` set `st` = 0 where `st` = 1;
  RETURN `new_id`;
END$$

DELIMITER ;

/* Определение для функции `f_country8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8add`(
        `pid` BIGINT(20),
        `country` VARCHAR(50),
        `language` VARCHAR(50)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_country` 
  (
    `id`,
    `pid`,
    `country`,
    `language`)  
  VALUES(
    @pn_id,
    pid,
    country,
    language);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_country8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_country` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_country8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8upd`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `country` VARCHAR(50),
        `language` VARCHAR(50)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_country` c
  SET 
    c.`pid` = pid,
    c.`country` = country,
    c.`language` = language 
  WHERE 
    c.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_country8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8mod`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `country` VARCHAR(50),
        `language` VARCHAR(50)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare
    pn_id  bigint;
  IF (id is not null) then
  	SELECT `f_country8upd`(id, pid, country, language) INTO @pn_id;
   else
    SELECT `f_country8add`(pid, country, language) INTO @pn_id;
  end if;
  return @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_country_group8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8add`(
        `caption` VARCHAR(300)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE 
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_country_group`
  (
    `id`,
    `caption`) 
  VALUE (
    @pn_id,
    caption);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_country_group8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_country_group` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_country_group8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8upd`(
        `id` BIGINT(20),
        `caption` VARCHAR(300)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_country_group` g
  SET 
    g.`caption` = caption 
  WHERE 
    g.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_country_group8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8mod`(
        `id` BIGINT(20),
        `caption` VARCHAR(300)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare 
    pn_id  bigint;
  IF (id is not null) then
    select `f_country_group8upd`(id, caption) into @pn_id;
   else
    select `f_country_group8add`(caption) into @pn_id;
  end if;
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_dictionary8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8add`(
        `pid` BIGINT(20),
        `parent` BIGINT(20),
        `caption` VARCHAR(3000),
        `description` VARCHAR(3000),
        `bibliography` VARCHAR(3000),
        `foreign_lang` VARCHAR(300)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_dictionary`
  (
    `id`,
    `pid`,
    `parent`,
    `caption`,
    `description`,
    `bibliography`,
    `foreign_lang`) 
  VALUES(
    @pn_id,
    pid,
    parent,
    caption,
    description,
    bibliography,
    foreign_lang);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_dictionary8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_dictionary` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_dictionary8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8upd`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `parent` BIGINT(20),
        `caption` VARCHAR(3000),
        `description` VARCHAR(3000),
        `bibliography` VARCHAR(3000),
        `foreign_lang` VARCHAR(300)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_dictionary` d
  SET 
    d.`pid` = pid,
    d.`parent` = parent,
    d.`caption` = caption,
    d.`description` = description,
    d.`bibliography` = bibliography,
    d.`foreign_lang` = foreign_lang 
  WHERE 
    d.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_dictionary8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8mod`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `parent` BIGINT(20),
        `caption` VARCHAR(3000),
        `description` VARCHAR(3000),
        `bibliography` VARCHAR(3000),
        `foreign_lang` VARCHAR(300)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare 
    pn_id  bigint;
  IF (id is not null) then
    select `f_dictionary8upd`(id, pid, parent, caption, description, bibliography, foreign_lang) into @pn_id;
   else
    select `f_dictionary8add`(pid, parent, caption, description, bibliography, foreign_lang) into @pn_id;
   end if;
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_files8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8add`(
        `pid` BIGINT,
        `caption` VARCHAR(300),
        `author` VARCHAR(300),
        `link` VARCHAR(3000),
        `visible` BOOLEAN,
        `bibliography` VARCHAR(3000),
        `type` VARCHAR(30),
        `key_word` VARCHAR(3000),
        `format` VARCHAR(30)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_files`
  (
    `id`,
    `pid`,
    `caption`,
    `author`,
    `link`,
    `add_date`,
    `visible`,
    `bibliography`,
    `type`,
    `key_word`,
    `format`) 
  VALUES (
    @pn_id,
    pid,
    caption,
    author,
    link,
    CURRENT_TIMESTAMP(),
    visible,
    bibliography,
    type,
    key_word,
    format);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_files8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_files` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_files8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8upd`(
        `id` BIGINT,
        `pid` BIGINT,
        `caption` VARCHAR(300),
        `author` VARCHAR(300),
        `link` VARCHAR(3000),
        `visible` BOOLEAN,
        `bibliography` VARCHAR(3000),
        `type` VARCHAR(30),
        `key_word` VARCHAR(3000),
        `firmat` VARCHAR(30)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_files` f  
  SET 
    f.`id` = id,
    f.`pid` = pid,
    f.`caption` = caption,
    f.`author` = author,
    f.`link` = link,
    f.`add_date` = CURRENT_TIMESTAMP(),
    f.`visible` = visible,
    f.`bibliography` = bibliography,
    f.`type` = type,
    f.`key_word` = key_word,
    f.`format` = format 
  WHERE 
    f.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_files8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8mod`(
        `id` BIGINT,
        `pid` BIGINT,
        `caption` VARCHAR(300),
        `author` VARCHAR(300),
        `link` VARCHAR(3000),
        `bibliography` VARCHAR(3000),
        `type` VARCHAR(30),
        `key_word` VARCHAR(3000),
        `format` VARCHAR(30)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare
    pn_id  bigint;
  IF (id is not null) then
  	SELECT `f_files8upd`(id,
    pid,
    caption,
    author,
    link,
    true,
    bibliography,
    type,
    key_word,
    format) INTO @pn_id;
   else
    SELECT `f_files8add`(pid,
    caption,
    author,
    link,
    true,
    bibliography,
    type,
    key_word,
    format) INTO @pn_id;
  end if;
  return @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_idiom8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8add`(
        `pid` BIGINT,
        `idiom` VARCHAR(3000),
        `original` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare 
  	pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_idiom`
  (
    `id`,
    `pid`,
    `idiom`,
    `original`) 
  VALUES (
    @pn_id,
    pid,
    idiom,
    original);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_idiom8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_idiom` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_idiom8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8upd`(
        `id` BIGINT,
        `pid` BIGINT,
        `idiom` VARCHAR(3000),
        `original` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_idiom` i
  SET 
    i.`pid` = pid,
    i.`idiom` = idiom,
    i.`original` = original 
  WHERE 
  	i.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_idiom8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8mod`(
        `id` BIGINT,
        `pid` BIGINT,
        `idiom` VARCHAR(3000),
        `original` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare  
    pn_id  bigint;
  IF (id is not null) then
  	SELECT `f_idiom8upd`(id, pid, idiom, original) INTO @pn_id;
   else
    SELECT `f_idiom8add`(pid, idiom, original) INTO @pn_id;
  end if;
  return @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_opus8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8add`(
        `pid` BIGINT(20),
        `caption` VARCHAR(300),
        `annotation` VARCHAR(3000),
        `cover` VARCHAR(300),
        `published` DATE,
        `bibliography` VARCHAR(3000),
        `key_word` VARCHAR(3000),
        `section` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE 
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_opus`
  (
    `id`,
    `pid`,
    `caption`,
    `annotation`,
    `cover`,
    `published`,
    `bibliography`,
    `key_word`,
    `section`) 
  VALUE (
    @pn_id,
    pid,
    caption,
    annotation,
    cover,
    published,
    bibliography,
    key_word,
    section);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_opus8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_opus` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_opus8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8upd`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `caption` VARCHAR(300),
        `annotation` VARCHAR(3000),
        `cover` VARCHAR(300),
        `published` DATE,
        `bibliography` VARCHAR(3000),
        `key_word` VARCHAR(3000),
        `section` BIGINT(20)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_opus` o
  SET 
    o.`pid` = pid,
    o.`caption` = caption,
    o.`annotation` = annotation,
    o.`cover` = cover,
    o.`published` = published,
    o.`bibliography` = bibliography,
    o.`key_word` = key_word,
    o.`section` = section 
  WHERE 
    o.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_opus8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8mod`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `caption` VARCHAR(300),
        `annotation` VARCHAR(3000),
        `cover` VARCHAR(300),
        `published` DATE,
        `bibliography` VARCHAR(3000),
        `key_word` VARCHAR(3000),
        `section` BIGINT(20)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare 
    pn_id  bigint;
  IF (id is not null) then
    select `f_opus8upd`(id, pid, caption, annotation, cover, published, bibliography, key_word, section) into @pn_id;
   else
    select `f_opus8add`(pid, caption, annotation, cover, published, bibliography, key_word, section) into @pn_id;
  end if;
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_person8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8add`(
        `name` VARCHAR(30),
        `surname` VARCHAR(30),
        `last_name` VARCHAR(30),
        `birth_date` DATE,
        `death_date` DATE,
        `country` BIGINT(20),
        `psrent` BIGINT(20),
        `school` BIGINT(20),
        `key_word` VARCHAR(3000),
        `annotation` VARCHAR(3000),
        `link` VARCHAR(3000),
        `portrait` VARCHAR(3000),
        `bibliography` VARCHAR(3000)
    )
    RETURNS TINYINT(4)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_person`
  (
    `id`,
    `name`,
    `surname`,
    `last_name`,
    `birth_date`,
    `death_date`,
    `country`,
    `parent`,
    `school`,
    `key_word`,
    `annotation`,
    `link`,
    `portrait`,
    `bibliography`) 
  VALUE (
    @pn_id,
    name,
    surname,
    last_name,
    birth_date,
    death_date,
    country,
    parent,
    school,
    key_word,
    annotation,
    link,
    portrait,
    bibliography);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_person8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_person` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_person8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8upd`(
        `id` BIGINT(20),
        `name` VARCHAR(30),
        `surname` VARCHAR(30),
        `last_name` VARCHAR(30),
        `birth_date` DATE,
        `death_date` DATE,
        `country` BIGINT(20),
        `parent` BIGINT(20),
        `school` BIGINT(20),
        `key_word` VARCHAR(3000),
        `annotation` VARCHAR(3000),
        `link` VARCHAR(3000),
        `portrait` VARCHAR(3000),
        `bibliography` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_person`  p
  SET 
    p.`name` = name,
    p.`surname` = surname,
    p.`last_name` = last_name,
    p.`birth_date` = birth_date,
    p.`death_date` = death_date,
    p.`country` = country,
    p.`parent` = parent,
    p.`school` = school,
    p.`key_word` = key_word,
    p.`annotation` = annotation,
    p.`link` = link,
    p.`portrait` = portrait,
    p.`bibliography` = bibliography 
  WHERE 
    p.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_person8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8mod`(
        `id` BIGINT(20),
        `name` VARCHAR(30),
        `surname` VARCHAR(30),
        `last_name` VARCHAR(30),
        `birth_date` DATE,
        `death_date` DATE,
        `country` BIGINT(20),
        `parent` BIGINT(20),
        `school` BIGINT(20),
        `key_word` VARCHAR(3000),
        `annotation` VARCHAR(3000),
        `link` VARCHAR(3000),
        `portrait` VARCHAR(3000),
        `bibliography` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare
    pn_id  bigint;
  IF (id is not null) then
    select `f_person8upd`(id, name, surname, last_name, birth_date, death_date, country, parent, school, key_word, annotation, link, portrait, bibliography) into @pn_id;
   else
    select `f_person8add`(name, surname, last_name, birth_date, death_date, country, parent, school, key_word, annotation, link, portrait, bibliography) into @pn_id;
  end if;
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_school8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8add`(
        `school` VARCHAR(50),
        `annotation` VARCHAR(3000),
        `parent` BIGINT,
        `bibliography` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_school`
  (
    `id`,
    `school`,
    `annotation`,
    `parent`,
    `bibliography`) 
  VALUE (
    @pn_id,
    school,
    annotation,
    parent,
    bibliography);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_school8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8del`(
        `pn_id` BIGINT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_school` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_school8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8upd`(
        `id` BIGINT(20),
        `school` VARCHAR(30),
        `annotation` VARCHAR(3000),
        `parent` BIGINT(20),
        `bibliography` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_school` s
  SET 
    s.`school` = school,
    s.`annotation` = annotation,
    s.`parent` = parent,
    s.`bibliography` = bibliography 
  WHERE 
    s.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_school8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8mod`(
        `id` BIGINT(20),
        `school` VARCHAR(30),
        `annotation` VARCHAR(3000),
        `parent` BIGINT(20),
        `bibliography` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare 
    pn_id  bigint;
  IF (id is not null) then
    select `f_school8upd`(id, school, annotation, parent, bibliography) into @pn_id;
   else
    select `f_school8add`(school, annotation, parent, bibliography) into @pn_id;
  end if;
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_section8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8add`(
        `pid` BIGINT,
        `caption` VARCHAR(300),
        `annotation` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE
    pn_id  bigint;
  SELECT `f_core_get_id`() INTO @pn_id;
  INSERT INTO 
    `t_section`
  (
    `id`,
    `pid`,
    `caption`,
    `annotation`) 
  VALUE (
    @pn_id,
    pid,
    caption,
    annotation);
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_section8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8del`(
        `pn_id` FLOAT
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DELETE FROM 
    `t_section` 
  WHERE 
    `id` = pn_id;
  RETURN pn_id;
END$$

DELIMITER ;

/* Определение для функции `f_section8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8upd`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `caption` VARCHAR(300),
        `annotation` VARCHAR(3000)
    )
    RETURNS BIGINT(20)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  UPDATE 
    `t_section` s
  SET 
    s.`pid` = pid,
    s.`caption` = caption,
    s.`annotation` = annotation 
  WHERE 
    s.`id` = id;
  RETURN id;
END$$

DELIMITER ;

/* Определение для функции `f_section8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8mod`(
        `id` BIGINT(20),
        `pid` BIGINT(20),
        `caption` VARCHAR(300),
        `annotation` VARCHAR(3000)
    )
    RETURNS TINYINT(4)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  declare
    pn_id  bigint;
  IF (id is not null) then
    select `f_section8upd`(id, pid, caption, annotation) into @pn_id;
   else
    select `f_section8add`(pid, caption, annotation) into @pn_id;
  end if;
  RETURN @pn_id;
END$$

DELIMITER ;

/* Определение для представления `v_country`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_country`
AS
select
  `t`.`id` AS `id`,
  `t`.`pid` AS `pid`,
  `t`.`country` AS `country`,
  `t`.`language` AS `language`,
  `t2`.`country` AS `pid_country`,
  `t2`.`language` AS `pid_language`
from
  (`t_country` `t`
  left join `t_country` `t2` on ((`t`.`pid` = `t2`.`id`)));

/* Определение для представления `v_country_group`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_country_group`
AS
select
  `cg`.`id` AS `id`,
  `cg`.`caption` AS `caption`
from
  `t_country_group` `cg`;

/* Определение для представления `v_dictionary`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_dictionary`
AS
select
  `d`.`id` AS `id`,
  `d`.`pid` AS `pid`,
  `d`.`parent` AS `parent`,
  `d`.`caption` AS `caption`,
  `d`.`description` AS `description`,
  `d`.`bibliography` AS `bibliography`,
  `d`.`foreign_lang` AS `foreign_lang`,
  `d2`.`caption` AS `parent_caption`,
  `d2`.`description` AS `parent_description`,
  `p`.`name` AS `name`,
  `p`.`surname` AS `surname`
from
  ((`t_dictionary` `d`
  left join `t_dictionary` `d2` on ((`d`.`parent` = `d2`.`id`)))
  left join `t_person` `p` on ((`d`.`pid` = `p`.`id`)));

/* Определение для представления `v_files`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_files`
AS
select
  `f`.`id` AS `id`,
  `f`.`pid` AS `pid`,
  `f`.`caption` AS `caption`,
  `f`.`author` AS `author`,
  `f`.`link` AS `link`,
  `f`.`add_date` AS `add_date`,
  `f`.`visible` AS `visible`,
  `f`.`bibliography` AS `bibliography`,
  `f`.`type` AS `type`,
  `f`.`key_word` AS `key_word`,
  `f`.`format` AS `format`
from
  (`t_files` `f`
  left join `t_opus` `o` on ((`f`.`pid` = `o`.`id`)));

/* Определение для представления `v_idiom`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_idiom`
AS
select
  `i`.`id` AS `id`,
  `i`.`pid` AS `pid`,
  `i`.`idiom` AS `idiom`,
  `i`.`original` AS `original`,
  `p`.`name` AS `name`
from
  (`t_idiom` `i`
  left join `t_person` `p` on ((`i`.`pid` = `p`.`id`)));

/* Определение для представления `v_opus`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_opus`
AS
select
  `t`.`id` AS `id`,
  `t`.`pid` AS `pid`,
  `t`.`caption` AS `caption`,
  `t`.`annotation` AS `annotation`,
  `t`.`cover` AS `cover`,
  `t`.`published` AS `published`,
  `t`.`bibliography` AS `bibliography`,
  `t`.`key_word` AS `key_word`,
  `t`.`section` AS `section`,
  `t2`.`name` AS `name`,
  `t2`.`surname` AS `surname`,
  `t3`.`caption` AS `section_caption`
from
  ((`t_opus` `t`
  left join `t_person` `t2` on ((`t`.`pid` = `t2`.`id`)))
  left join `t_section` `t3` on ((`t`.`section` = `t3`.`id`)));

/* Определение для представления `v_person`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_person`
AS
select
  `p`.`id` AS `id`,
  `p`.`name` AS `name`,
  `p`.`surname` AS `surname`,
  `p`.`last_name` AS `last_name`,
  `p`.`birth_date` AS `birth_date`,
  `p`.`death_date` AS `death_date`,
  `p`.`country` AS `country_id`,
  `p`.`parent` AS `papa_person_id`,
  `p`.`school` AS `school_id`,
  `p`.`annotation` AS `person_annotation`,
  `p`.`link` AS `link`,
  `p`.`portrait` AS `portrait`,
  `p`.`bibliography` AS `person_bibliography`,
  `p`.`key_word` AS `person_key_word`
from
  `t_person` `p`;

/* Определение для представления `v_school`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_school`
AS
select
  `t`.`id` AS `id`,
  `t`.`school` AS `school`,
  `t`.`annotation` AS `annotation`,
  `t`.`parent` AS `parent`,
  `t`.`bibliography` AS `bibliography`,
  `t2`.`school` AS `parent_school`
from
  (`t_school` `t`
  left join `t_school` `t2` on ((`t`.`parent` = `t2`.`id`)));

/* Определение для представления `v_section`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_section`
AS
select
  `s`.`id` AS `id`,
  `s`.`pid` AS `pid`,
  `s`.`caption` AS `caption`,
  `s`.`annotation` AS `annotation`,
  `s2`.`caption` AS `pid_caption`,
  `s2`.`annotation` AS `pid_annotation`
from
  (`t_section` `s`
  left join `t_section` `s2` on ((`s`.`pid` = `s2`.`id`)));

/* Definition for the `t_country_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_after_ins_tr1` AFTER INSERT ON `t_country`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Страны" добавлена запись ', 
    coalesce(NEW.country, 'нет название'),' (', 
    NEW.id,'-',
    coalesce(NEW.pid, 'нет группы'), '-',
    coalesce(NEW.language, 'нет языка'), ')'), 
    'country', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (', 
    NEW.id,',',
    coalesce(NEW.pid, ''),',',
    coalesce(NEW.country, ''),',',
    coalesce(NEW.language, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_country_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_after_upd_tr1` AFTER UPDATE ON `t_country`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Страны" изменена запись ', 
    OLD.country,' (', 
    OLD.id,'-', 
    coalesce(OLD.pid, 'нет группы'), '-', 
    OLD.language, ') на ', 
    NEW.country, ' (', 
    NEW.id,'-',
    coalesce(NEW.pid, 'нет группы'), '-',
     NEW.language, ')'), 
    'country', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (', 
    OLD.id,',',
    coalesce(OLD.pid, ''),',',
    OLD.country,',',
    OLD.language,')'));
END$$

DELIMITER ;

/* Definition for the `t_country_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_after_del_tr1` AFTER DELETE ON `t_country`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Страны" удалена запись ', 
    OLD.country,' (', 
    OLD.id,'-', 
    coalesce(OLD.pid, 'нет группы'), '-',
     OLD.language, ')'),  
    'country', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_country(id, \r\n    pid,\r\n    country, \r\n    language) values (', 
    OLD.id,',',
    coalesce(OLD.pid, ''),',',
    OLD.country,',',
    OLD.language,')'));
END$$

DELIMITER ;

/* Definition for the `t_country_group_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_group_after_ins_tr1` AFTER INSERT ON `t_country_group`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Группы стран" добавлена запись ', 
    NEW.caption,' (', 
    NEW.id, ')'), 
    'country_group', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_country_group(id,\r\n     caption) values (', 
     NEW.id,',',
     NEW.caption,')'));
END$$

DELIMITER ;

/* Definition for the `t_country_group_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_group_after_upd_tr1` AFTER UPDATE ON `t_country_group`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Группы стран" изменена запись ', 
    OLD.caption,' (', 
    OLD.id, ') на ', 
    NEW.caption, ' (', 
    NEW.id, ')'), 
    'country_group', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_country_group(id, \r\n    caption) values (', 
    OLD.id,',',
    OLD.caption,
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_country_group_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_group_after_del_tr1` AFTER DELETE ON `t_country_group`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Группы стран" удалена запись ', 
    OLD.caption,' (', 
    OLD.id, ')'), 
    'country_group', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_country_group(id, \r\n    caption) values (', 
    OLD.id,',',
    OLD.caption,')'));
END$$

DELIMITER ;

/* Definition for the `t_dictionary_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_dictionary_after_ins_tr1` AFTER INSERT ON `t_dictionary`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Словарь" добавлена запись ', 
    NEW.caption, ' (', 
    NEW.id,'-',
    coalesce(NEW.pid,'нет автора'), '-', 
    COALESCE(NEW.parent, 'нет предыдущего понятия'), '-',
    coalesce(NEW.description, 'нет описания'),'-',
    coalesce(NEW.bibliography, 'нет библиографии'),'-',
    coalesce(NEW.foreign_lang, 'нет перевода'),
    ')'),  
    'dictionary', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (', 
    NEW.id,',',
    coalesce(NEW.pid,''),',', 
    COALESCE(NEW.parent, ''),',',
    coalesce(NEW.description, ''),',',
    coalesce(NEW.bibliography, ''),',',
    coalesce(NEW.foreign_lang, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_dictionary_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_dictionary_after_upd_tr1` AFTER UPDATE ON `t_dictionary`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Словарь" изменена запись ', 
    OLD.caption,' (', 
    OLD.id,'-',
    coalesce(OLD.pid,'нет автора'), '-', 
    COALESCE(OLD.parent, 'нет предыдущего понятия'),'-',
    coalesce(OLD.description, 'нет описания'),'-',
    coalesce(OLD.bibliography, 'нет библиографии'),'-',
    coalesce(OLD.foreign_lang, 'нет перевода'),
    ') на ',
    NEW.caption,' (', 
    NEW.id,'-',
    coalesce(NEW.pid,'нет автора'), '-', 
    COALESCE(NEW.parent, 'нет предыдущего понятия'),'-',
    coalesce(NEW.description, 'нет описания'),'-',
    coalesce(NEW.bibliography, 'нет библиографии'),'-',
    coalesce(NEW.foreign_lang, 'нет перевода'), ')'
      ),  
    'dictionary', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (', 
    OLD.id,',',
    coalesce(OLD.pid,''), ',', 
    COALESCE(OLD.parent, ''),',',
    OLD.description,',',
    OLD.bibliography,',',
    OLD.foreign_lang,
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_dictionary_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_dictionary_after_del_tr1` AFTER DELETE ON `t_dictionary`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Словарь" удалена запись ', 
    OLD.caption,' (', 
    OLD.id,'-',
    coalesce(OLD.pid,'нет автора'),'-', 
    COALESCE(OLD.parent, 'нет предыдущего понятия'),'-',
    OLD.description,'-',
    OLD.bibliography,'-',
    OLD.foreign_lang,
    ')'),  
    'dictionary', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (', 
    OLD.id,',',
    coalesce(OLD.pid,''), ',', 
    COALESCE(OLD.parent, ''),',',
    OLD.description,',',
    OLD.bibliography,',',
    OLD.foreign_lang,
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_files_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_files_after_ins_tr1` AFTER INSERT ON `t_files`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Файлы" добавлена запись ', 
    coalesce(NEW.caption, 'без названия'),' (', 
    coalesce(NEW.id, ''),'-',
    coalesce(NEW.pid, 'без связи'),'-',
    coalesce(NEW.author, 'без автора'),'-',
    coalesce(NEW.link, 'без ссылки'),'-',
    coalesce(NEW.visible, 'без статуса'),'-',
    coalesce(NEW.bibliography, 'без библиографии'),'-',
    coalesce(NEW.type, 'без типа'),'-',
    coalesce(NEW.key_word, 'без ключевых слов'),'-',
    coalesce(NEW.format, 'без расширения'),')'), 
    'files', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (', 
    coalesce(NEW.id, ''),',',
    coalesce(NEW.pid, ''),',',
    coalesce(NEW.caption, ''),',',
    coalesce(NEW.author, ''),',',
    coalesce(NEW.link, ''),',',
    coalesce(NEW.visible, ''),',',
    coalesce(NEW.bibliography, ''),',',
    coalesce(NEW.type, ''),',',
    coalesce(NEW.key_word, ''),',',
    coalesce(NEW.format, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_files_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_files_after_upd_tr1` AFTER UPDATE ON `t_files`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Файлы" изменена запись ', 
    coalesce(OLD.caption, 'без названия'),' (', 
    coalesce(OLD.id, ''),'-',
    coalesce(OLD.pid, 'без связи'),'-',
    coalesce(OLD.author, 'без автора'),'-',
    coalesce(OLD.link, 'без ссылки'),'-',
    coalesce(OLD.visible, 'без статуса'),'-',
    coalesce(OLD.bibliography, 'без библиографии'),'-',
    coalesce(OLD.type, 'без типа'),'-',
    coalesce(OLD.key_word, 'без ключевых слов'),'-',
    coalesce(OLD.format, 'без расширения'),') на ', 
    coalesce(NEW.caption, 'без названия'),' (', 
    coalesce(NEW.id, ''),'-',
    coalesce(NEW.pid, 'без связи'),'-',
    coalesce(NEW.author, 'без автора'),'-',
    coalesce(NEW.link, 'без ссылки'),'-',
    coalesce(NEW.visible, 'без статуса'),'-',
    coalesce(NEW.bibliography, 'без библиографии'),'-',
    coalesce(NEW.type, 'без типа'),'-',
    coalesce(NEW.key_word, 'без ключевых слов'),'-',
    coalesce(NEW.format, 'без расширения'),')'),
    'files', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (', 
    coalesce(OLD.id, ''),',',
    coalesce(OLD.pid, ''),',',
    coalesce(OLD.caption, ''),',',
    coalesce(OLD.author, ''),',',
    coalesce(OLD.link, ''),',',
    coalesce(OLD.visible, ''),',',
    coalesce(OLD.bibliography, ''),',',
    coalesce(OLD.type, ''),',',
    coalesce(OLD.key_word, ''),',',
    coalesce(OLD.format, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_files_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_files_after_del_tr1` AFTER DELETE ON `t_files`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Файлы" удалена запись ', 
    coalesce(OLD.caption, 'без названия'),' (', 
    coalesce(OLD.id, ''),'-',
    coalesce(OLD.pid, 'без связи'),'-',
    coalesce(OLD.author, 'без автора'),'-',
    coalesce(OLD.link, 'без ссылки'),'-',
    coalesce(OLD.visible, 'без статуса'),'-',
    coalesce(OLD.bibliography, 'без библиографии'),'-',
    coalesce(OLD.type, 'без типа'),'-',
    coalesce(OLD.key_word, 'без ключевых слов'),'-',
    coalesce(OLD.format, 'без расширения'),')'), 
    'files', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (', 
    coalesce(OLD.id, ''),',',
    coalesce(OLD.pid, ''),',',
    coalesce(OLD.caption, ''),',',
    coalesce(OLD.author, ''),',',
    coalesce(OLD.link, ''),',',
    coalesce(OLD.visible, ''),',',
    coalesce(OLD.bibliography, ''),',',
    coalesce(OLD.type, ''),',',
    coalesce(OLD.key_word, ''),',',
    coalesce(OLD.format, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_idiom_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_idiom_after_ins_tr1` AFTER INSERT ON `t_idiom`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Высказывания" добавлена запись ', 
    NEW.idiom,' (', 
    NEW.id,'-', 
    coalesce(NEW.pid, 'нет автора'), '-',
    coalesce(NEW.original, 'нет оригинала'), ')'),  
    'idiom', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (', 
    NEW.id,',',
    coalesce(NEW.pid, ''),',',
    coalesce(NEW.idiom, ''),',',
    coalesce(NEW.original, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_idiom_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_idiom_after_upd_tr1` AFTER UPDATE ON `t_idiom`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Высказывания" изменена запись ', 
    OLD.idiom,' (', 
    OLD.id,'-', 
    coalesce(OLD.pid, 'нет автора'), '-', 
    coalesce(OLD.original, 'нет оригинала'), ') на ', 
    NEW.idiom, ' (', 
    NEW.id,'-',
    coalesce(NEW.pid, 'нет автора'), '-',
     coalesce(NEW.original ,'нет оригинала'), ')'), 
    'idiom', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_idiom(id, \r\n    pid, \r\n    idiom, \r\n    original) values (', 
    OLD.id,',',
    coalesce(OLD.pid, ''),',',
    coalesce(OLD.idiom, ''),',',
    coalesce(OLD.original, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_idiom_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_idiom_after_del_tr1` AFTER DELETE ON `t_idiom`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Высказывания" удалена запись ', 
    OLD.idiom,' (', 
    OLD.id,'-', 
    coalesce(OLD.pid, 'нет автора'), '-',
    coalesce(OLD.original, 'нет оригинала'), ')'),  
    'idiom', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (', 
    OLD.id,',',
    coalesce(OLD.pid, ''),',',
    coalesce(OLD.idiom, ''),',',
    coalesce(OLD.original, ''),')'));
END$$

DELIMITER ;

/* Definition for the `t_opus_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_opus_after_ins_tr1` AFTER INSERT ON `t_opus`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Произведения" добавлена запись ', 
    NEW.caption,' (', 
    NEW.id, '-',
    coalesce(NEW.pid, 'нет автора'), '-',
    coalesce(NEW.caption, 'нет названия'), '-',
    coalesce(NEW.annotation, 'нет аннотации'), '-',
    coalesce(NEW.cover, 'нет обложки'), '-',
    coalesce(NEW.published, 'нет даты'), '-',
    coalesce(NEW.bibliography, 'нет библиографии'), '-',
    coalesce(NEW.key_word, 'нет ключей'), '-',
    coalesce(NEW.section, 'нет раздела'),
    ')'), 
    'opus', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (', 
    NEW.id, ',',
    coalesce(NEW.pid, ''), ',',
    coalesce(NEW.caption, ''), ',',
    coalesce(NEW.annotation, ''), ',',
    coalesce(NEW.cover, ''), ',',
    coalesce(NEW.published, ''), ',',
    coalesce(NEW.bibliography, ''), ',',
    coalesce(NEW.key_word, ''), ',',
    coalesce(NEW.section, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_opus_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_opus_after_upd_tr1` AFTER UPDATE ON `t_opus`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Произведения" изменена запись ', 
    OLD.caption,' (', 
    OLD.id, '-',
    coalesce(OLD.pid, 'нет автора'), '-',
    coalesce(OLD.caption, 'нет названия'), '-',
    coalesce(OLD.annotation, 'нет аннотации'), '-',
    coalesce(OLD.cover, 'нет обложки'), '-',
    coalesce(OLD.published, 'нет даты'), '-',
    coalesce(OLD.bibliography, 'нет библиографии'), '-',
    coalesce(OLD.key_word, 'нет ключей'), '-',
    coalesce(OLD.section, 'нет раздела'),
    ') на ',
    NEW.caption, '(',
    coalesce(NEW.id, ''), '-',
    coalesce(NEW.pid, 'нет автора'), '-',
    coalesce(NEW.annotation, 'нет аннотации'), '-',
    coalesce(NEW.cover, 'нет обложки'), '-',
    coalesce(NEW.published, 'нет даты'), '-',
    coalesce(NEW.bibliography, 'нет библиографии'), '-',
    coalesce(NEW.key_word, 'нет ключей'), '-',
    coalesce(NEW.section, 'нет раздела'), ')'
    ), 
    'opus', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (', 
    OLD.id, ',',
    coalesce(OLD.pid, ''), ',',
    coalesce(OLD.caption, ''), ',',
    coalesce(OLD.annotation, ''), ',',
    coalesce(OLD.cover, ''), ',',
    coalesce(OLD.published, ''), ',',
    coalesce(OLD.bibliography, ''), ',',
    coalesce(OLD.key_word, ''), ',',
    coalesce(OLD.section, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_opus_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_opus_after_del_tr1` AFTER DELETE ON `t_opus`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Произведения" удалена запись ', OLD.caption,' (', 
    OLD.id, '-',
    coalesce(OLD.pid, 'нет автора'), '-',
    coalesce(OLD.caption, 'нет названия'), '-',
    coalesce(OLD.annotation, 'нет аннотации'), '-',
    coalesce(OLD.cover, 'нет обложки'), '-',
    coalesce(OLD.published, 'нет даты'), '-',
    coalesce(OLD.bibliography, 'нет библиографии'), '-',
    coalesce(OLD.key_word, 'нет ключей'), '-',
    coalesce(OLD.section, 'нет раздела'),
    ')'), 
    'opus', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (', 
    OLD.id, ',',
    coalesce(OLD.pid, ''), ',',
    coalesce(OLD.caption, ''), ',',
    coalesce(OLD.annotation, ''), ',',
    coalesce(OLD.cover, ''), ',',
    coalesce(OLD.published, ''), ',',
    coalesce(OLD.bibliography, ''), ',',
    coalesce(OLD.key_word, ''), ',',
    coalesce(OLD.section, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_person_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_person_after_ins_tr1` AFTER INSERT ON `t_person`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Персоны" добавлена запись ', 
    NEW.name,' (', 
    NEW.id, '-',
    coalesce(NEW.surname, 'нет фамилии'), '-',
    coalesce(NEW.last_name, 'нет отчества'), '-',
    coalesce(NEW.birth_date, 'нет рождения'), '-',
    coalesce(NEW.death_date, 'нет смерти'), '-',
    coalesce(NEW.country, 'нет страны'), '-', 
    coalesce(NEW.parent, 'нет предшественника'), '-',
    coalesce(NEW.school, 'нет школы'), '-',
    coalesce(NEW.key_word, 'нет ключей'), '-',
    coalesce(NEW.annotation, 'нет аннотации'), '-',
    coalesce(NEW.link, 'нет подробностей'), '-',
    coalesce(NEW.portrait, 'нет портрета'), '-',
    coalesce(NEW.bibliography, 'нет библиографии'), '-'
    ')'), 
    'person', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (', 
    NEW.id, ',',
    coalesce(NEW.name, ''), ',',
    coalesce(NEW.surname, ''), ',',
    coalesce(NEW.last_name, ''), ',',
    coalesce(NEW.birth_date, ''), ',',
    coalesce(NEW.death_date, ''), ',',
    coalesce(NEW.country, ''), ',', 
    coalesce(NEW.parent, ''), ',',
    coalesce(NEW.school, ''), ',',
    coalesce(NEW.key_word, ''), ',',
    coalesce(NEW.annotation, ''), ',',
    coalesce(NEW.link, ''), ',',
    coalesce(NEW.portrait, ''), ',',
    coalesce(NEW.bibliography, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_person_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_person_after_upd_tr1` AFTER UPDATE ON `t_person`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Персоны" изменена запись ', 
    OLD.name,' (', 
    OLD.id, '-',
    coalesce(OLD.surname, 'нет фамилии'), '-',
    coalesce(OLD.last_name, 'нет отчества'), '-',
    coalesce(OLD.birth_date, 'нет рождения'), '-',
    coalesce(OLD.death_date, 'нет смерти'), '-',
    coalesce(OLD.country, 'нет страны'), '-', 
    coalesce(OLD.parent, 'нет предшественника'), '-',
    coalesce(OLD.school, 'нет школы'), '-',
    coalesce(OLD.key_word, 'нет ключей'), '-',
    coalesce(OLD.annotation, 'нет аннотации'), '-',
    coalesce(OLD.link, 'нет подробностей'), '-',
    coalesce(OLD.portrait, 'нет портрета'), '-',
    coalesce(OLD.bibliography, 'нет библиографии'), ' на ',
    NEW.name,' (', 
    NEW.id, '-',
    coalesce(NEW.surname, 'нет фамилии'), '-',
    coalesce(NEW.last_name, 'нет отчества'), '-',
    coalesce(NEW.birth_date, 'нет рождения'), '-',
    coalesce(NEW.death_date, 'нет смерти'), '-',
    coalesce(NEW.country, 'нет страны'), '-', 
    coalesce(NEW.parent, 'нет предшественника'), '-',
    coalesce(NEW.school, 'нет школы'), '-',
    coalesce(NEW.key_word, 'нет ключей'), '-',
    coalesce(NEW.annotation, 'нет аннотации'), '-',
    coalesce(NEW.link, 'нет подробностей'), '-',
    coalesce(NEW.portrait, 'нет портрета'), '-',
    coalesce(NEW.bibliography, 'нет библиографии'),
    ')'), 
    'person', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (', 
    OLD.id, ',',
    coalesce(OLD.name, ''), ',',
    coalesce(OLD.surname, ''), ',',
    coalesce(OLD.last_name, ''), ',',
    coalesce(OLD.birth_date, ''), ',',
    coalesce(OLD.death_date, ''), ',',
    coalesce(OLD.country, ''), ',', 
    coalesce(OLD.parent, ''), ',',
    coalesce(OLD.school, ''), ',',
    coalesce(OLD.key_word, ''), ',',
    coalesce(OLD.annotation, ''), ',',
    coalesce(OLD.link, ''), ',',
    coalesce(OLD.portrait, ''), ',',
    coalesce(OLD.bibliography, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_person_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_person_after_del_tr1` AFTER DELETE ON `t_person`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Персоны" удалена запись ', 
    OLD.name,' (', 
    OLD.id, '-',
    coalesce(OLD.surname, 'нет фамилии'), '-',
    coalesce(OLD.last_name, 'нет отчества'), '-',
    coalesce(OLD.birth_date, 'нет рождения'), '-',
    coalesce(OLD.death_date, 'нет смерти'), '-',
    coalesce(OLD.country, 'нет страны'), '-', 
    coalesce(OLD.parent, 'нет предшественника'), '-',
    coalesce(OLD.school, 'нет школы'), '-',
    coalesce(OLD.key_word, 'нет ключей'), '-',
    coalesce(OLD.annotation, 'нет аннотации'), '-',
    coalesce(OLD.link, 'нет подробностей'), '-',
    coalesce(OLD.portrait, 'нет портрета'), '-',
    coalesce(OLD.bibliography, 'нет библиографии'), '-'
    ')'), 
    'person', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (', 
    OLD.id, ',',
    coalesce(OLD.name, ''), ',',
    coalesce(OLD.surname, ''), ',',
    coalesce(OLD.last_name, ''), ',',
    coalesce(OLD.birth_date, ''), ',',
    coalesce(OLD.death_date, ''), ',',
    coalesce(OLD.country, ''), ',', 
    coalesce(OLD.parent, ''), ',',
    coalesce(OLD.school, ''), ',',
    coalesce(OLD.key_word, ''), ',',
    coalesce(OLD.annotation, ''), ',',
    coalesce(OLD.link, ''), ',',
    coalesce(OLD.portrait, ''), ',',
    coalesce(OLD.bibliography, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_school_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_school_after_ins_tr1` AFTER INSERT ON `t_school`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Школы" добавлена запись ', 
    coalesce(NEW.school, 'нет названия'),' (', 
    coalesce(NEW.id, ''), '-',
    coalesce(NEW.annotation, 'нет аннотации'), '-',
    coalesce(NEW.parent, 'нет предшественника'), '-',
    coalesce(NEW.bibliography, 'нет библиографии'), 
    ')'), 
    'school', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (', 
    coalesce(NEW.id, ''), ',',
    coalesce(NEW.school, ''),',', 
    coalesce(NEW.annotation, ''), ',',
    coalesce(NEW.parent, ''), ',',
    coalesce(NEW.bibliography, ''), 
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_school_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_school_after_upd_tr1` AFTER UPDATE ON `t_school`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Школы" изменена запись ', 
    coalesce(OLD.school, 'нет названия'),' (', 
    coalesce(OLD.id, ''), '-',
    coalesce(OLD.annotation, 'нет аннотации'), '-',
    coalesce(OLD.parent, 'нет предщественника'), '-',
    coalesce(OLD.bibliography, 'нет библиографии'), 
    ') на ',
    coalesce(NEW.school, 'нет названия'),' (', 
    coalesce(NEW.id, ''), '-',
    coalesce(NEW.annotation, 'нет аннотации'), '-',
    coalesce(NEW.parent, 'нет предшественника'), '-',
    coalesce(NEW.bibliography, 'нет библиографии'), ')'
    ), 
    'school', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (', 
    coalesce(OLD.id, ''), ',',
    coalesce(OLD.school, ''),',', 
    coalesce(OLD.annotation, ''), ',',
    coalesce(OLD.parent, ''), ',',
    coalesce(OLD.bibliography, ''), 
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_school_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_school_after_del_tr1` AFTER DELETE ON `t_school`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Школы" удалена запись ', 
    coalesce(OLD.school, 'нет названия'),' (', 
    coalesce(OLD.id, ''), '-',
    coalesce(OLD.annotation, 'нет аннотации'), '-',
    coalesce(OLD.parent, 'нет предщественника'), '-',
    coalesce(OLD.bibliography, 'нет библиографии'), 
    ')'), 
    'school', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (', 
    coalesce(OLD.id, ''), ',',
    coalesce(OLD.school, ''),',', 
    coalesce(OLD.annotation, ''), ',',
    coalesce(OLD.parent, ''), ',',
    coalesce(OLD.bibliography, ''), 
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_section_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_section_after_ins_tr1` AFTER INSERT ON `t_section`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    NEW.id, 
    CONCAT('В таблицу "Разделы науки" добавлена запись ', 
    NEW.caption,' (', 
    NEW.id,'-',
    coalesce(NEW.pid, 'нет надраздела'),'-',
    coalesce(NEW.caption, 'нет названия'),'-',
    coalesce(NEW.annotation, 'нет аннотации'),
    ')'), 
    'section', 
    CURRENT_TIMESTAMP(), 
    'i', 
    CONCAT('insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (', 
    NEW.id,',',
    coalesce(NEW.pid, ''),',',
    coalesce(NEW.caption, ''),',',
    coalesce(NEW.annotation, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_section_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_section_after_upd_tr1` AFTER UPDATE ON `t_section`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('В таблице "Разделы науки" изменена запись ', 
    OLD.caption,' (', 
    OLD.id,'-',
    coalesce(OLD.pid, 'нет надраздела'),'-',
    coalesce(OLD.caption, 'нет названия'),'-',
    coalesce(OLD.annotation, 'нет аннотации'),
    ') на ',
    NEW.caption,' (', 
    NEW.id,'-',
    coalesce(NEW.pid, 'нет надраздела'),'-',
    coalesce(NEW.caption, 'нет названия'),'-',
    coalesce(NEW.annotation, 'нет аннотации'), ')'
    ), 
    'section', 
    CURRENT_TIMESTAMP(), 
    'u', 
    CONCAT('insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (', 
    OLD.id,',',
    coalesce(OLD.pid, ''),',',
    coalesce(OLD.caption, ''),',',
    coalesce(OLD.annotation, ''),
    ')'));
END$$

DELIMITER ;

/* Definition for the `t_section_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_section_after_del_tr1` AFTER DELETE ON `t_section`
  FOR EACH ROW
BEGIN
	INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) 
    VALUES(`f_core_get_id`(), 
    OLD.id, 
    CONCAT('Из таблицы "Разделы науки" удалена запись ', 
    OLD.caption,' (', 
    OLD.id,'-',
    coalesce(OLD.pid, 'нет надраздела'),'-',
    coalesce(OLD.caption, 'нет названия'),'-',
    coalesce(OLD.annotation, 'нет аннотации'),
    ')'), 
    'section', 
    CURRENT_TIMESTAMP(), 
    'd', 
    CONCAT('insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (', 
    OLD.id,',',
    coalesce(OLD.pid, ''),',',
    coalesce(OLD.caption, ''),',',
    coalesce(OLD.annotation, ''),
    ')'));
END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;