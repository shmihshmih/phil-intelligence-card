//редактирование файлов
function editFiles(files_id) {
    toggleBlock("files_new");

    var http = createObject();
    http.open("POST","m/m_files.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_files="+files_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_files = JSON.parse(http.response);

            var files_id = document.querySelector(['[name="files_id"]']);
            var files_pid = document.querySelector(['[name="files_pid"]']);
            var files_caption = document.querySelector(['[name="files_caption"]']);
            var files_author = document.querySelector(['[name="files_author"]']);
            var files_link = document.querySelector(['[name="files_link"]']);
            var files_bibliography = document.querySelector(['[name="files_bibliography"]']);
            var files_type = document.querySelector(['[name="files_type"]']);
            var files_key_word = document.querySelector(['[name="files_key_word"]']);
            var files_format = document.querySelector(['[name="files_format"]']);
            
            files_id.value = exist_files['files_id'];
            files_pid.value = exist_files['files_pid'];
            files_caption.value = exist_files['files_caption'];
            files_author.value = exist_files['files_author'];
            files_link.value = exist_files['files_link'];
            files_bibliography.value = exist_files['files_bibliography'];
            files_type.value = exist_files['files_type'];
            files_key_word.value = exist_files['files_key_word'];
            files_format.value = exist_files['files_format'];
        }
    }
}
//удаление файла
function delFiles(id) {
    var del = confirm('Вы дествительно хотите удалить файл с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_files.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=files&files_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}