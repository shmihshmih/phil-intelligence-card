  //уникальные значения массива
function unique(arr) {
  var result = [];
  nextInput:
    for (var i = 0; i < arr.length; i++) {
      var str = arr[i]; // для каждого элемента
      for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
        if (result[j] == str) continue nextInput; // если да, то следующий
      }
      result.push(str);
    }
  return result;
}

//получим все персоны
function getAllPersons() {
  //алё тут переменная
  var country=[];
  var http = createObject();
  //насчёт запросов и аяксов, если будет двоиться, то раздделить
  http.open("POST","v/v_visual.php",true);
  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  http.send("visual=person");

  http.onreadystatechange = function() {
    if (http.readyState === 4 && http.status === 200) {

      var persons = JSON.parse(http.response);

      for (var i = 0; i < persons['id'].length; i++) {
        //сюда зальём левый столбец(страны кароч)
        country.push(persons['country_caption'][i])
      }

      country = unique(country);

  //левый столбец страны
  var groups = new vis.DataSet();
  for (var g = 0; g < country.length; g++) {
    groups.add({id: country[g], content: country[g]});
  }
  //правый основной столбец
  var items = new vis.DataSet();
  for (var i = 0; i < persons['id'].length; i++) {
    //нужно правильно расставить группы(тут страна выставляется)
    if(persons['school_caption'][i]) {
      var school_caption = persons['school_caption'][i];
    } else {
      var school_caption = '-';
    }
  
    items.add({
      id: i,
      group: persons['country_caption'][i],
      content: persons['name'][i] +
          ' <span style="color:#97B0F8;">(' + school_caption + ')</span>',
      start: persons['birth_date'][i],
      type: 'box'
    });
  }
  
   //инициируем визуализацию
  var container = document.getElementById('visualization');
  var now = new Date();
  var options = {
    start: '0000-01-01',
    end: now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate(),
    editable: false,
    groupOrder: 'content'
  };
  
  var timeline = new vis.Timeline(container);
  timeline.setOptions(options);
  timeline.setGroups(groups);
  timeline.setItems(items);
    }
  }
}
getAllPersons();