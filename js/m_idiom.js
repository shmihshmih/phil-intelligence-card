//редактирование высказываний
function editIdiom(idiom_id) {
    toggleBlock("idiom_new");

    var http = createObject();
    http.open("POST","m/m_idiom.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_idiom="+idiom_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_idiom = JSON.parse(http.response);

            var idiom_id = document.querySelector(['[name="idiom_id"]']);
            var idiom_pid = document.querySelector(['[name="idiom_pid"]']);
            var idiom_idiom = document.querySelector(['[name="idiom_idiom"]']);
            var idiom_original = document.querySelector(['[name="idiom_original"]']);
            
            idiom_id.value = exist_idiom['idiom_id'];
            idiom_pid.value = exist_idiom['idiom_pid'];
            idiom_idiom.value = exist_idiom['idiom_idiom'];
            idiom_original.value = exist_idiom['idiom_original'];
        }
    }
}
//удаление высказывания
function delIdiom(id) {
    var del = confirm('Вы дествительно хотите удалить высказывание с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_idiom.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=idiom&idiom_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}