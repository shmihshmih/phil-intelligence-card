//редактирование школы
function editSchool(school_id) {
    toggleBlock("school_new");

    var http = createObject();
    http.open("POST","m/m_school.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_school="+school_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_school = JSON.parse(http.response);

            var school_id = document.querySelector(['[name="school_id"]']);
            var school_caption = document.querySelector('[name="school_caption"]');
            var school_annotation = document.querySelector('[name="school_annotation"]');
            var school_parent = document.querySelector('[name="school_parent"]');
            var school_bibliography = document.querySelector('[name="school_bibliography"]');

            school_id.value = exist_school['school_id'];
            school_caption.value = exist_school['school_caption'];
            school_annotation.value = exist_school['school_annotation'];
            school_parent.value = exist_school['school_parent'];
            school_bibliography.value = exist_school['school_bibliography'];
        }
    }
}
//удаление школы
function delSchool(id) {
    var del = confirm('Вы дествительно хотите удалить школу с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_school.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=school&school_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}
