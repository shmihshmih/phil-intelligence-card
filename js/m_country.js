//редактирование стран
function editCountry(country_id) {
    toggleBlock("country_new");

    var http = createObject();
    http.open("POST","m/m_country.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_country="+country_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_country = JSON.parse(http.response);

            var country_id = document.querySelector(['[name="country_id"]']);
            var country_group = document.querySelector(['[name="country_group"]']);
            var country_caption = document.querySelector('[name="country_caption"]');
            var country_language = document.querySelector('[name="language"]');

            country_id.value = exist_country['country_id'];
            country_group.value = exist_country['country_group'];
            country_caption.value = exist_country['country_caption'];
            country_language.value = exist_country['country_language'];
        }
    }
}
//удаление стран
function delCountry(id) {
    var del = confirm('Вы дествительно хотите удалить страну с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_country.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=country&country_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}