//редактирование раздела науки
function editSection(section_id) {
    toggleBlock("section_new");

    var http = createObject();
    http.open("POST","m/m_section.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_section="+section_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_section = JSON.parse(http.response);

            var section_id = document.querySelector(['[name="section_id"]']);
            var section_parent = document.querySelector('[name="section_parent"]');
            var section_caption = document.querySelector('[name="section_caption"]');
            var section_annotation = document.querySelector('[name="section_annotation"]');

            section_id.value = exist_section['section_id'];
            section_parent.value = exist_section['section_parent'];
            section_caption.value = exist_section['section_caption'];
            section_annotation.value = exist_section['section_annotation'];
        }
    }
}
//удаление раздела науки
function delSection(id) {
    var del = confirm('Вы дествительно хотите удалить раздел науки с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_section.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=section&section_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}