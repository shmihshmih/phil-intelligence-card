//редактирование персоны
function editPerson(person_id) {
    toggleBlock("person_new");
    console.log(person_id);

    var http = createObject();
    console.log('inner ajax');
    http.open("POST","m/m_person.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_person="+person_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {

            var exist_person = JSON.parse(http.response);

            var person_id = document.querySelector('[name="person_id"]');
            var person_name = document.querySelector('[name="person_name"]');
            var person_surname = document.querySelector('[name="person_surname"]');
            var person_last_name = document.querySelector('[name="person_last_name"]');
            var person_birth_date = document.querySelector('[name="person_birth_date"]');
            var person_death_date = document.querySelector('[name="person_death_date"]');
            var person_country_list = document.querySelector('[name="person_country_list"]');
            var person_person_list = document.querySelector('[name="person_person_list"]');
            var person_school_list = document.querySelector('[name="person_school_list"]');
            var person_key_word = document.querySelector('[name="person_key_word"]');
            var person_annotation = document.querySelector('[name="person_annotation"]');
            var person_link = document.querySelector('[name="person_link"]');
            var person_portrait = document.querySelector('[name="person_portrait"]');
            var person_bibliography = document.querySelector('[name="person_bibliography"]');

            person_id.value = exist_person['id'];
            person_name.value = exist_person['name'];
            person_surname.value = exist_person['surname'];
            person_last_name.value = exist_person['last_name'];
            person_birth_date.value = exist_person['birth_date'];
            person_death_date.value = exist_person['death_date'];
            person_country_list.value = exist_person['country'];
            person_person_list.value = exist_person['parent'];
            person_school_list.value = exist_person['school'];
            person_key_word.value = exist_person['key_word'];
            person_annotation.value = exist_person['annotation'];
            person_link.value = exist_person['link'];
            person_portrait.value = exist_person['portrait'];
            person_bibliography.value = exist_person['bibliography'];
        };
    }
}
//удаление персоны
function delPerson(id) {
    var del = confirm('Вы действительно хотите удалить запись с идентификатором '+id+'?');
    console.log('before ajax');

    if(del == true) {
        var http = createObject();
        console.log('inner ajax');
        http.open("POST","m/m_person.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=person&person_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}
