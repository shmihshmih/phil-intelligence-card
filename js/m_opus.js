//редактирование произведения
function editOpus(opus_id) {
    toggleBlock("opus_new");
    
    var http = createObject();
    http.open("POST","m/m_opus.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_opus="+opus_id);
    
    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_opus = JSON.parse(http.response);

            var opus_id = document.querySelector(['[name="opus_id"]']);
            var opus_pid = document.querySelector('[name="opus_pid"]');
            var opus_caption = document.querySelector('[name="opus_caption"]');
            var opus_annotation = document.querySelector('[name="opus_annotation"]');
            var opus_cover = document.querySelector('[name="opus_cover"]');
            var opus_published = document.querySelector('[name="opus_published"]');
            var opus_bibliography = document.querySelector('[name="opus_bibliography"]');
            var opus_key_word = document.querySelector('[name="opus_key_word"]');
            var section_opus_list = document.querySelector('[name="section_opus_list"]');

            opus_id.value = exist_opus['opus_id'];
            opus_pid.value = exist_opus['opus_pid'];
            opus_caption.value = exist_opus['opus_caption'];
            opus_annotation.value = exist_opus['opus_annotation'];
            opus_cover.value = exist_opus['opus_cover'];
            opus_published.value = exist_opus['opus_published'];
            opus_bibliography.value = exist_opus['opus_bibliography'];
            opus_key_word.value = exist_opus['opus_key_word'];
            section_opus_list.value = exist_opus['opus_section'];
        }
    }
}
//удаление произведения
function delOpus(id) {
     var del = confirm('Вы дествительно хотите удалить произведение с идентификатором '+id+'?');   
    
    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_opus.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=opus&opus_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}