//переключалка добавление/редактирование
function toggleBlock(el) {
	//находим элемент
	var block = document.getElementById(el);
	//разделяем на основную и добавочную часть
	var main_part = block.id.substring(0,block.id.lastIndexOf('_')+1);
	var add_part = block.id.substr(block.id.lastIndexOf('_')+1);

	//находим оба элемента
	var edit_el = document.getElementById(main_part+'edit');
	var new_el = document.getElementById(main_part+'new');

	//находим стили обоих элементов
	var edit_el_display = getComputedStyle(edit_el).display;
	var new_el_display = getComputedStyle(new_el).display;

	//переключаем
	if(add_part == 'new') {
		new_el.style.display = 'block';
		edit_el.style.display = 'none';
	}

	if(add_part == 'edit') {
		new_el.style.display = 'none';
		edit_el.style.display = 'block';
	}
}
//XHR обьект
function createObject() {
    var request_type;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        request_type = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        request_type = new XMLHttpRequest();
    }
    return request_type;
}
//очистить форму
function cleaner(el) {
    document.getElementById(el).reset();
}
//поменять вкладку у формы
function tabForm(el) {
	//находим элемент
	var block = document.querySelector("."+el);
    //находим видимость
    var el_status = getComputedStyle(block).display;
    //меняем видимость
    if(el_status == "none") {
        block.style.display = 'block';
    } else {
        block.style.display = 'none';
    }
}