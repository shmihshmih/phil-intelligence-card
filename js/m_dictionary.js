//редактирование понятия
function editDictionary(dictionary_id) {
    toggleBlock("dictionary_new");

    var http = createObject();
    http.open("POST","m/m_dictionary.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_dictionary="+dictionary_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_dictionary = JSON.parse(http.response);

            var dictionary_id = document.querySelector(['[name="dictionary_id"]']);
            var person_dictionary_list = document.querySelector(['[name="person_dictionary_list"]']);
            var dictionary_dictionary_list = document.querySelector(['[name="dictionary_dictionary_list"]']);
            var dictionary_caption = document.querySelector(['[name="dictionary_caption"]']);
            var dictionary_description = document.querySelector(['[name="dictionary_description"]']);
            var dictionary_bibliography = document.querySelector(['[name="dictionary_bibliography"]']);
            var foreign_lang = document.querySelector(['[name="foreign_lang"]']);
            
            dictionary_id.value = exist_dictionary['dictionary_id'];
            person_dictionary_list.value = exist_dictionary['dictionary_pid'];
            dictionary_dictionary_list.value = exist_dictionary['dictionary_parent'];
            dictionary_caption.value = exist_dictionary['dictionary_caption'];
            dictionary_description.value = exist_dictionary['dictionary_description'];
            dictionary_bibliography.value = exist_dictionary['dictionary_bibliography'];
            foreign_lang.value = exist_dictionary['dictionary_foreign_lang'];
        }
    }
}
//удаление понятия
function delDictionary(id) {
    var del = confirm('Вы дествительно хотите удалить word с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_dictionary.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=dictionary&dictionary_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}