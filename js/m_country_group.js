//редактирование группы стран
function editCountry_group(country_group_id) {
    toggleBlock("country_group_new");

    var http = createObject();
    http.open("POST","m/m_country_group.php",true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send("choose_country_group="+country_group_id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var exist_country_group = JSON.parse(http.response);

            var country_group_id = document.querySelector(['[name="country_group_id"]']);
            var country_group_caption = document.querySelector('[name="country_group_caption"]');

            country_group_id.value = exist_country_group['country_group_id'];
            country_group_caption.value = exist_country_group['country_group_caption'];
        }
    }
}
//удаление группы стран
function delCountry_group(id) {
    var del = confirm('Вы дествительно хотите удалить группу стран с идентификатором '+id+'?');

    if(del == true) {
        var http = createObject();
        http.open("POST","m/m_country_group.php",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.send("del_kind=country_group&country_group_id="+id);

    http.onreadystatechange = function() {
        if (http.readyState === 4 && http.status === 200) {
            var errorText = JSON.parse(http.response);
            if(errorText[2] == null) {
                location.href=location.href;
            } else {
                alert(errorText[2]);
            }
        };
    };
    }
}