<?php
//получение главного списка разделов науки
$stmt = $connect->prepare("SELECT v.id, v.caption from $db.v_section v order by v.caption");
$stmt->execute();
while($row = $stmt->fetch()) {
    echo "<div class='list_item'><div class='button_select' onclick='editSection(".
        $row['id'].")'>".
        $row['caption']."</div><div class='button_delete' onclick='delSection(".
        $row['id'].");'></div></div>";
}
?>