<?php
//получение главного списка стран
$stmt = $connect->prepare("SELECT v.id, v.country from $db.v_country v order by v.country");
$stmt->execute();
while($row = $stmt->fetch()) {
    echo "<div class='list_item'><div class='button_select' onclick='editCountry(".
        $row['id'].")'>".
        $row['country']."</div><div class='button_delete' onclick='delCountry(".
        $row['id'].");'></div></div>";
}
?>