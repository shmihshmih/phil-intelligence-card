<?php
 require_once('../db.php');
 $data = null;
    $stmt = $connect->prepare("SELECT * from $db.v_person");
    $stmt->execute();
    while($row = $stmt->fetch()) {
    	$data['id'][] = $row['id'];
        $data['name'][] = $row['name'];
        $data['surname'][] = $row['surname'];
        $data['last_name'][] = $row['last_name'];
        $data['birth_date'][] = $row['birth_date'];
        $data['death_date'][] = $row['death_date'];
        $data['country_id'][] = $row['country_id'];
        $data['papa_person_id'][] = $row['papa_person_id'];
        $data['school_id'][] = $row['school_id'];
        $data['person_annotation'][] = $row['person_annotation'];
        $data['link'][] = $row['link'];
        $data['portrait'][] = $row['portrait'];
        $data['person_bibliography'][] = $row['person_bibliography'];
        $data['person_key_word'][] = $row['person_key_word'];
        $data['country_caption'][] = $row['country_caption'];
        $data['school_caption'][] = $row['school_caption'];
    };
    print_r(json_encode($data))
?>